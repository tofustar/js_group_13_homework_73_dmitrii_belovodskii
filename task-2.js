const express = require('express');
const Vigenere = require('caesar-salad').Vigenere;
const app = express();
const port = 8000;

const password = 'password';

app.get('/encode/:something', (req, res) => {
    const encode = Vigenere.Cipher(password).crypt(`${req.params.something}`);
    res.send(`${req.params.something} to encode: ${encode}`);
});

app.get('/decode/:something', (req, res) => {
    const decode = Vigenere.Decipher(password).crypt(`${req.params.something}`);
    res.send(`${req.params.something} to decode: ${decode}`);
});

app.listen(port, () => {
    console.log('We are live on ' + port);
});